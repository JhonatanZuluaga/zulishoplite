<?php

require "tests/config.php";
class ClienteCest 
{
    public function _before(FunctionalTester $I)
    {
    }

    public function _after(FunctionalTester $I)
    {
    }
    
    // tests
    public function test1(){
        
        //\Codeception\Test\Unit::assertEquals("0",0);
        //\Codeception\Util\Debug::debug("Client created: ");
        
        
    }
    
    public function testCreate(){
        $client = new Client(null, "Jhon", "1234", "jhon@doe.com");
        $r = $client->create();

        \Codeception\Test\Unit::assertEquals($r["error"],0);
        
        $lclient = Client::getById($r["getID"]);
        
        \Codeception\Util\Debug::debug("Client created: ");
        \Codeception\Util\Debug::debug($lclient);
        
        \Codeception\Test\Unit::assertEquals(get_class($lclient),"Client");
    }
    
    public function testRead(){
        
       
        $client = Client::getBy("email", "jhon@doe.com");
        
        $id = $client->getId();
        $username = $client->getUsername();
        $password = $client->getPassword();
        $email = $client->getEmail();
        
        \Codeception\Test\Unit::assertEquals($username,"Jhon");
        \Codeception\Test\Unit::assertEquals($password,"1234");
        \Codeception\Test\Unit::assertEquals($email,"jhon@doe.com");
        
        
        \Codeception\Util\Debug::debug("Cliente read: ");
        \Codeception\Util\Debug::debug($username);
        
       
        
    }
    
    public function testUpdate(){
        
       
        $client = Client::getBy("email", "jhon@doe.com");
        
        $client->setUsername("dionisio");
        $r = $client->update();
        \Codeception\Test\Unit::assertEquals($r["error"],0);
        
        $client->setPassword("4321");
        $r = $client->update();
        \Codeception\Test\Unit::assertEquals($r["error"],0);
        
        $client->setEmail("dionisioo@gmail.com");
        $r = $client->update();
        \Codeception\Test\Unit::assertEquals($r["error"],0);
        
        $client->setId(100);
        $r = $client->update();
        \Codeception\Test\Unit::assertEquals($r["error"],0);
        
        $lclient = Client::getBy("email", "jhon@doe.com");
        
        \Codeception\Util\Debug::debug("Client Update: ");
        \Codeception\Util\Debug::debug($lclient);
        
        \Codeception\Test\Unit::assertEquals($lclient,null);
        
    }
    
    public function testDelete(){
        
       
        $client = Client::getBy("email", "dionisioo@gmail.com");    
        $response = Clients_bl::eliminar($client);
        
        if ($response) {
            
            $r = ["error" => 0, "msg" => "Eliminado"];
            
        } else {
            $r = ["error" => 1, "msg" => "No se pudo eliminar"];
        }
        
        \Codeception\Test\Unit::assertEquals($r["error"],0);
        
        $lclient = Client::getBy("email", "jhon@doe.com");
        \Codeception\Test\Unit::assertEquals($lclient,null);
        
        \Codeception\Util\Debug::debug("Client Delete: ");
        \Codeception\Util\Debug::debug($lclient);
        
    }
    
}
