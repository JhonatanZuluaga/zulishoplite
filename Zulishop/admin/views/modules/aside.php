<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar"> 
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php print(URL); ?>public/dist/img/user2-160x160.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php print($this->user->getUsername()); ?></p>
        <span><i class="fa fa-user-circle text-red"></i>	&nbsp;<?php print $this->user->rolDetail->getRol();?></span>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      
     <?php foreach ($this->menus as $key => $menu) : ?>
        <!-- treeview -->
      <li class="<?php if($key == 0) { echo "active";} ?><!--treeview-->">
          <a class="asyncLink" href="<?php print URL.$menu->getUrl(); ?>"><i class="fa fa-circle-o text-red"></i> <span><?php print $menu->getName(); ?></span></a>
        <!--a href="#">
          <i class="fa fa-heartbeat"></i> <span><?php print $menu->getName(); ?></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a-->
        
        <!--ul class="treeview-menu">
          <li class="active">
            <a class="asyncLink" href="<?php print URL.$menu->getUrl(); ?>" >
              <i class="fa fa-circle-o"></i> Registrar
            </a>
          </li>
          
          <li>
            <a class="asyncLink" href="<?php print(URL); ?>Evaluation/search/" >
              <i class="fa fa-circle-o"></i> Buscar
            </a>
          </li>
          
        </ul-->
        
      </li>
      
      <?php endforeach; ?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
