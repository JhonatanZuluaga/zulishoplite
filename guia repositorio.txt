git clone @direcci�n del repo
git status
git remote show origin -> ver el origen del repositorio

(NUNCA HACER CHECKOUT DEL MASTER)
(SI YA SE HIZO MERGE CON MASTER, LA BRANCH SE BORRA Y DEBE VOLVER A CREARLA)
git checkout (nombre branch) -> si no existe creela con:
	- git checkout -b (nombre branch)
	- git push --set-upstream origin (nombre branch)

agregar archivos:
git checkout (nombre branch)
git add -A
git commit -m "informaci�n de lo que hizo en este commit"
git push -u origin (nombre branch)


(Si el master tiene archivos nuevos, los agregar� sin borrar los archivos ya existentes en su carpeta, pero, si uno de los archivos del master contiene modificaciones de uno que usted tenga, se reemplazar� por el del master.)

para bajar la �ltima versi�n del master:
git pull origin master

(si los miembros modificaron un mismo archivo, saldr� un error, debe guardar una copia de los cambios en otra parte local y posteriormente hacer un:)
git fetch --all
git reset --hard origin/master
